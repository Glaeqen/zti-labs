package zti;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import model.Person;

@ManagedBean(name = "servJPADB2Bean")
@RequestScoped
public class ServJPADB2 {

    // private Person person;
    private ConnJPA baza;

    public ServJPADB2() {
        baza = new ConnJPA("PU_DB2");
    }

    public List<Person> getPeople() {
        List<Person> people = baza.getPersonList();
        return people;
    }
}
