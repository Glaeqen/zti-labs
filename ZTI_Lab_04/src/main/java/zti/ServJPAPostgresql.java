package zti;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import model.Person;

@ManagedBean(name = "servJPAPostgresqlBean")
@SessionScoped
public class ServJPAPostgresql {

    private Person person;
    private Person editPerson;
    private Person newPerson;
    private ConnJPA baza;

    public ServJPAPostgresql() {
        System.out.println("Init Persitance Unit");
        baza = new ConnJPA("PU_Postgresql");
        person = new Person();
        editPerson = new Person();
        newPerson = new Person();
    }

    public List<Person> getPeople() {
        List<Person> people = baza.getPersonList();
        return people;
    }

    public String delPerson(Person entity) {
        baza.deletePerson(entity);
        return "allRecPostgresql" ;
    }

    public Person getPerson() {
        return person;
    }

    public Person getEditPerson() {
        return editPerson;
    }

    public Person getNewPerson() {
        return newPerson;
    }

    public String selectPerson(Person entity) {
        person = copy(entity);
        return "viewRecPostgresql" ;
    }

    public String updPerson(Person entity) {
        editPerson = copy(entity);
        System.out.println("updPerson -- " + editPerson.getLname());
        return "updRecPostgresql" ;
    }

    private Person copy(Person entity) {
        person = new Person();
        person.setId(entity.getId());
        person.setFname(entity.getFname());
        person.setLname(entity.getLname());
        person.setCity(entity.getCity());
        person.setEmail(entity.getEmail());
        person.setTel(entity.getTel());
        return person ;
    }


    public String savePerson() {
        Person entity = newPerson;
        System.out.println("[ServJPA] Save entity - " + entity.getLname() );
        baza.savePerson(entity);
        newPerson = new Person();
        return "allRecPostgresql";
    }

    public String updatePerson() {
        baza.updatePerson(editPerson);
        editPerson = new Person();
        return "allRecPostgresql";
    }

}
