package zti;

public class ConfJDBC {
    private Boolean setJNDI = false ;
    private String driver;
    private String user;
    private String pass;
    private String urlJDBC;
    private String nameJNDI;

    public void setDriver(String newValue) {
        driver = newValue;
    }

    public String getDriver() {
        return driver;
    }

    public void setUser(String newValue) {
        user = newValue;
    }

    public String getUser() {
        return user;
    }

    public void setPass(String newValue) {
        pass = newValue;
    }

    public String getPass() {
        return pass;
    }

    public void setUrlJDBC(String newValue) {
        urlJDBC = newValue;
    }

    public String getUrlJDBC() {
        return urlJDBC;
    }


    public void setSetJNDI(Boolean newValue) {
        setJNDI = newValue;
    }

    public Boolean getSetJNDI() {
        return setJNDI;
    }

    public void setNameJNDI(String newValue) {
        nameJNDI = newValue;
    }

    public String getNameJNDI() {
        return nameJNDI;
    }

}