package zti;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnJDBC {

    private Person0 person;
    private Connection conn = null;
    private PreparedStatement prestmt = null;
    private Statement stmt = null;
    private ResultSet rset = null;
    private List<Person0> list = new ArrayList<Person0>();

    public ConnJDBC(ConfJDBC config) {
        // System.out.println("Init managed Bean");
        if (config.getSetJNDI()) {
            String nameJNDI = config.getNameJNDI();
            try {
                Context context = new InitialContext();
                DataSource ds1 = (DataSource) context.lookup(nameJNDI);
                conn = ds1.getConnection();
            } catch (NamingException ex) {
                System.out.println(ex.getMessage());
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        } else {
            String driver = config.getDriver();
            String username = config.getUser();
            String password = config.getPass();
            String url = config.getUrlJDBC();
            try {
                Class.forName(driver);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            try {
                conn = DriverManager.getConnection(url, username, password);
                // System.out.println("Connect to Database");
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public List<Person0> getPersonList() {
        try {
            try {
                stmt = conn.createStatement();

                String sql = "SELECT * FROM person ORDER BY lname";
                rset = stmt.executeQuery(sql);
                while (rset.next()) {
                    person = new Person0();
                    person.setFname(rset.getString("fname"));
                    person.setLname(rset.getString("lname"));
                    person.setEmail(rset.getString("email"));
                    person.setTel(rset.getString("tel"));
                    person.setCity(rset.getString("city"));
                    person.setId(rset.getInt("id"));
                    list.add(person);
                }
                rset.close();
            } finally {
                if (prestmt != null) {
                    prestmt.close();
                }
                if (conn != null) {
                    System.out.println("Connection to database close");
                    conn.close();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public void createPerson(Person0 person) {
        try {
            try {
                prestmt = conn
                        .prepareStatement("INSERT INTO person (fname, lname, email, city, tel) VALUES (?,?,?,?, ?)");
                prestmt.setString(1, person.getFname());
                prestmt.setString(2, person.getLname());
                prestmt.setString(3, person.getEmail());
                prestmt.setString(4, person.getCity());
                prestmt.setString(5, person.getTel());
                prestmt.executeUpdate();
            } finally {
                if (prestmt != null) {
                    prestmt.close();
                }
                if (conn != null) {
                    System.out.println("Connection to database close");
                    conn.close();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return;
    }

    public Person0 readPerson(String id) {
        try {
            try {
                Integer ident = Integer.parseInt(id);
                prestmt = conn.prepareStatement("SELECT * FROM person WHERE id = ? ");
                prestmt.setInt(1, ident);
                ResultSet rset = prestmt.executeQuery();
                person = new Person0();
                while (rset.next()) {
                    person.setFname(rset.getString("fname"));
                    person.setLname(rset.getString("lname"));
                    person.setEmail(rset.getString("email"));
                    person.setTel(rset.getString("tel"));
                    person.setCity(rset.getString("city"));
                    person.setId(rset.getInt("id"));
                }

            } finally {
                if (prestmt != null) {
                    prestmt.close();
                }
                if (conn != null) {
                    System.out.println("Connection to database close");
                    conn.close();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return person;
    }

    public void updatePerson(Person0 person) {
        try {
            try {
                // System.out.println("Deleted record" + person.getId());
                prestmt = conn.prepareStatement("UPDATE person SET fname=?,lname=?,email=?,city=?,tel=?  WHERE id = ?");
                prestmt.setString(1, person.getFname());
                prestmt.setString(2, person.getLname());
                prestmt.setString(3, person.getEmail());
                prestmt.setString(4, person.getCity());
                prestmt.setString(5, person.getTel());
                prestmt.setInt(6, person.getId());
                prestmt.executeUpdate();
            } finally {
                if (prestmt != null) {
                    prestmt.close();
                }
                if (conn != null) {
                    System.out.println("Connection to database close");
                    conn.close();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return;
    }

    public void deletePerson(String id) {
        try {
            try {
                // System.out.println("Deleted record" + id);
                Integer ident = Integer.parseInt(id);
                prestmt = conn.prepareStatement("DELETE FROM person WHERE id = ?");
                prestmt.setInt(1, ident);
                prestmt.executeUpdate();
            } finally {
                if (prestmt != null) {
                    prestmt.close();
                }
                if (conn != null) {
                    System.out.println("Connection to database close");
                    conn.close();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return;
    }

}
