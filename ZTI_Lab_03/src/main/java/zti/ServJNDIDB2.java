package zti;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;

@ManagedBean(name = "servJNDIDB2Bean")
@RequestScoped
public class ServJNDIDB2 {


    private Person0 person ;
    private ConnJDBC baza;

    public ServJNDIDB2() {
        ConfJDBC conf = new ConfJDBC();
        conf.setSetJNDI(true);
        conf.setNameJNDI("jdbc/db2");
        baza = new ConnJDBC(conf) ;
    }

    public List<Person0> getPeople() {
        List<Person0> people = baza.getPersonList();
        return people;
    }


}
