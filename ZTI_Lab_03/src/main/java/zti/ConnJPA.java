package zti;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import model.Person;

public class ConnJPA {
    private EntityManagerFactory managerFactory; // = Persistence.createEntityManagerFactory(persistenceUnitName);
    private EntityManager entityManager; // = managerFactory.createEntityManager();
    private EntityTransaction entityTransaction;

    public ConnJPA(String persistenceUnitName ) {
        managerFactory = Persistence.createEntityManagerFactory(persistenceUnitName);
        entityManager = managerFactory.createEntityManager();
        entityTransaction = entityManager.getTransaction();
    }

    @SuppressWarnings("unchecked")
    public List<Person> getPersonList() {
        List<Person> people = null;
        try {
            people = (List<Person>) entityManager.createNamedQuery("findAll").getResultList();
            // manager.close();
        } catch (Exception e) {
            System.out.println("Failed !!! " + e.getMessage());
        }
        return people;
    }
    public void savePerson( Person entity) {
        //entityTransaction.begin();
        System.out.println("[ConnJPA] Save entity - " + entity.getLname() );
        entityTransaction.begin();
        entityManager.persist(entity);
        entityManager.flush();
        entityTransaction.commit();
    }

    public void updatePerson(Person entity) {
        entityTransaction.begin();
        entityManager.merge(entity);
        entityTransaction.commit();
    }

    public void deletePerson(Person entity) {
        System.out.println("Delete entity - " + entity.getLname() + " ID: " + entity.getId().toString());
        entityTransaction.begin();
        entityManager.remove(entity);
        entityManager.flush();
        entityTransaction.commit();
    }

    public Person findPerson(int id) {
        Person entity = (Person) entityManager.find(Person.class, id);
        return entity;
    }
}
