package zti;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import model.Person;

@ManagedBean(name = "servJPADB2Bean")
@RequestScoped
public class ServJPADB2 {

    // private Person person;
    private ConnJPA baza;
    private Person person;
    private Person editPerson;
    private Person newPerson;

    public ServJPADB2() {
        System.out.println("Init Persitance Unit");
        baza = new ConnJPA("PU_DB2");
        person = new Person();
        editPerson = new Person();
        newPerson = new Person();
    }

    public List<Person> getPeople() {
        List<Person> people = baza.getPersonList();
        return people;
    }

    public String delPerson(Person entity) {
        baza.deletePerson(entity);
        return "allRecDB2" ;
    }

    public Person getPerson() {
        return person;
    }

    public Person getEditPerson() {
        return editPerson;
    }

    public Person getNewPerson() {
        return newPerson;
    }

    public String selectPerson(Person entity) {
        person = copy(entity);
        return "viewRecDB2" ;
    }

    public String updPerson(Person entity) {
        editPerson = copy(entity);
        System.out.println("updPerson -- " + editPerson.getLname());
        return "updRecDB2" ;
    }

    private Person copy(Person entity) {
        person = new Person();
        person.setId(entity.getId());
        person.setFname(entity.getFname());
        person.setLname(entity.getLname());
        person.setCity(entity.getCity());
        person.setEmail(entity.getEmail());
        person.setTel(entity.getTel());
        return person ;
    }


    public String savePerson() {
        Person entity = newPerson;
        System.out.println("[ServJPA] Save entity - " + entity.getLname() );
        baza.savePerson(entity);
        newPerson = new Person();
        return "allRecDB2";
    }

    public String updatePerson() {
        baza.updatePerson(editPerson);
        editPerson = new Person();
        return "allRecDB2";
    }
}
