package zti;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;

@ManagedBean(name = "servJNDIPostgresqlBean")
@RequestScoped
public class ServJNDIPostgresql {

    private Person0 person ;
    private ConnJDBC baza;

    public ServJNDIPostgresql() {
        ConfJDBC conf = new ConfJDBC();
        conf.setSetJNDI(true);
        conf.setNameJNDI("jdbc/postgresql");
        baza = new ConnJDBC(conf) ;
    }

    public List<Person0> getPeople() {
        List<Person0> people = baza.getPersonList();
        return people;
    }

}
