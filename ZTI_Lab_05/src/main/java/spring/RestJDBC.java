package spring;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import model.Person;
import model.PersonDaoImpl;

@Controller(value = "JdbcController")
@RequestMapping("/rest")
public class RestJDBC {

    @Autowired
    PersonDaoImpl dao;

    @RequestMapping(value = "/person", method = RequestMethod.GET)
    public @ResponseBody List<Person> getAllPerson() {
        // logger.info("Start getAllEmployees.");
        List<Person> people = dao.getPeople() ;
        return people ;
    }

    @RequestMapping(value= "/person/{id}", method= RequestMethod.GET)
    public @ResponseBody Person getPerson(@PathVariable int id) {
        Person entity = dao.getPersonByID(id);
        return entity;
    }

}
