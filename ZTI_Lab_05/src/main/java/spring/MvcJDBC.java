package spring;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import model.Person;
import model.PersonDaoImpl;

@Controller(value = "MvcController")
@RequestMapping("/mvc")
public class MvcJDBC {

    @Autowired
    PersonDaoImpl personDao;

    @RequestMapping(value = "/list")
    public String getTable(Model model) {
        // logger.info("Start getAllEmployees.");
        List<Person> people = personDao.getPeople() ;
        model.addAttribute("table",people);
        return "list";
    }

}

